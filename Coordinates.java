package sample;

public class Coordinates {
    private Integer x;
    private Integer y;
    private Field field;

    final Rand random = new Rand();

    public Coordinates(){
        field = new Field();
        this.x = random.getInteger(field.getxSize());
        this.y = random.getInteger(field.getySize());
    }
    public void setX(Integer x) {
        this.x = x;
    }
    public void setY(Integer y) {
        this.y = y;
    }
    public Integer getX() {
        return x;
    }
    public Integer getY() {
        return y;
    }
    public boolean moveUp(){
        if (this.y == 0) {
            return true;
        }
        this.y = this.y - 1;
        return false;
    }
    public boolean moveDown() {
        if (this.y == this.field.getySize() - 1) {
            return true;
        }
        this.y = this.y + 1;
        return false;
    }
    public boolean moveLeft() {
        if (this.x == 0)
            return true;
        this.x = this.x - 1;
        return false;
    }
    public boolean moveRight() {
        if (this.x == this.field.getxSize() - 1) {
            return true;
        }
        this.x = this.x + 1;
        return false;
    }
}
